<?php


namespace App\Repositories;

use App\Post;
use App\Repositories\RepositoryTrait;


class PostRepository {

    use RepositoryTrait;

    public function __construct(Post $post)
    {
        $this->model = $post;
    }

    public function bySlug($slug)
    {
        return $this->model->findOrFail($slug);
    }

}
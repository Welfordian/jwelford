<?php


namespace App\Repositories;


trait RepositoryTrait {
    public function all()
    {
        return $this->model->all();
    }

    public function byId($id)
    {
        return $this->model->where('id', $id);
    }
}
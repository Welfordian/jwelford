<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model {

    public function scopeDefaultPermissions()
    {
        $config = $this->first()->default_permissions;

        return json_decode($config);
    }
}

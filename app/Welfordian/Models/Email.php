<?php

namespace Welfordian\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model {

    protected $fillable = [
        'subject', 'from', 'sender', 'body-html', 'body-plain', 'thread_id',
    ];

    public function getBodyHtmlAttribute($html)
    {
        if (! strlen($html))
        {
            if (strlen($this->getAttribute("body-plain")))
            {
                return $this->getAttribute("body-plain");
            } else
            {
                return "";
            }
        }
        libxml_use_internal_errors(true);
        $d = new \DOMDocument;
        $mock = new \DOMDocument;
        $d->loadHTML($html);
        $body = $d->getElementsByTagName('body')->item(0);
        foreach ($body->childNodes as $child)
        {
            $mock->appendChild($mock->importNode($child, true));
        }

        $html = $mock->saveHTML();
        $html = preg_replace("/src=\"([^\"]+)\"/", "src=\"https://jwelford.co.uk/utils/image/safe?url=$1\"", $html);
        $html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
        return $html;
    }
}

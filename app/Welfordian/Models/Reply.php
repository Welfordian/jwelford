<?php

namespace Welfordian\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;

class Reply extends Model {

    protected $fillable = [
        'from', 'body-html',
    ];

    public function parseInternalUser()
    {
        $user_id = $this->get_between($this->from, "internal_user_id(", ")");
        if ((int) $user_id === Auth::user()->id)
        {
            return "You";
        }

        return User::find($user_id)->name;
    }

    //This should really be removed...

    private function get_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;

        return substr($string, $ini, $len);
    }

}

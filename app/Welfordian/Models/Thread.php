<?php

namespace Welfordian\Models;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model {

    protected $fillable = [
        'subject', 'participant',
    ];

    public function mails()
    {
        return $this->hasMany(Email::class);
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function seen()
    {
        $this->seen = 1;
        $this->save();
    }

    public function getAvatar()
    {
        return "https://www.gravatar.com/avatar/" . md5(strtolower(trim($this->participant))) . "?s=40";

    }
}

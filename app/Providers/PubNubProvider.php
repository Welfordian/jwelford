<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PubNubProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Pubnub\Pubnub', function ($app) {
            return new \Pubnub\Pubnub([
                'subscribe_key' => 'sub-c-8e0178a2-5180-11e6-9baf-0619f8945a4f',
                'publish_key'   => 'pub-c-e83816fe-a78f-4fe5-828b-d5bb83249219',
                'ssl'           => false,
            ]);
        });
    }
}

<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function __construct()
    {
        $this->permissions = "{}";
    }

    public function hasPermission($master, $action)
    {
        $permissions = json_decode($this->permissions, true);
        if (! array_key_exists($master, $permissions))
        {
            return false;
        }

        if (! in_array($action, $permissions[ $master ]))
        {
            return false;
        }

        return true;
    }

    public function hasPermissions($which)
    {
        $permissions = json_decode($this->permissions, true);
        foreach ($which as $master => $sub)
        {
            if (! array_key_exists($master, $permissions))
            {
                return false;
            }

            if (! (count(array_intersect($permissions[ $master ], $which[ $master ])) === count($which[ $master ])))
            {
                return false;
            }
        }

        return true;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    protected $primaryKey = "slug";
    public $incrementing = false;
}

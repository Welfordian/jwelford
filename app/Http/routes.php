<?php

/*
 * Routes are placed in App/Http/Routes in folders
 * based on their respective request methods
 */

foreach ( File::allFiles(__DIR__.'/Routes') as $route_partial)
{
    require_once $route_partial->getPathName();
}

/*
 * This one's easy--Auth.
 */

Route::auth();
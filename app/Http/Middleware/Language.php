<?php

namespace App\Http\Middleware;

use Closure;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //This needs cleaning up, good God...
        $langs = ['en', 'de'];
        if(isset($_GET['lang']) && in_array($_GET['lang'], $langs)){
            $request->session()->put('language',$_GET['lang']);
        }

        $language = $request->session()->get('language','en'); //en will be the default language.
        \App::setlocale($language);

        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;
use View;
use App\Post;
use Welfordian\Models\Thread;
use Closure;

class Permissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct()
    {
        View::share([
            'nav_posts_count' => Post::count(),
            'nav_email_count' => Thread::where(['seen' => '0'])->count()
        ]);
    }

    public function handle($request, Closure $next)
    {
        $permissions = $this->getRequiredPermissionsForRoute($request->route());
        if(!$request->user()->hasPermissions($permissions)){
            return response(view('admin.insufficient_permissions'));
        }
        return $next($request);
    }

    public function getRequiredPermissionsForRoute($route)
    {
        $actions = $route->getAction();
        return isset($actions['required']) ? $actions['required'] : [];
    }
}

<?php

/*
 * All POST Requests for the /api URI
 */

Route::group(['middleware' => 'auth'], function () {

    Route::post('/api/email/search', 'ApiController@searchEmails');
});
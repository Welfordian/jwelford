<?php

/*
 * This file handles the routes for 3rd-party applications
 * that accept a URI for things like web-hooks etc...
 *
 * NOTE: All methods on the controllers must provide
 * their own validation on whether a request came
 * from the actual 3rd-part application or not
 */

Route::post('/mail/inbound', 'MailController@inbound');
<?php

/*
 * All POST Routes for the /admin URI
 */

Route::group(['middleware' => ['auth', 'permissions']], function ()
{
    Route::post('/admin/messages/{thread}', ['required' => [
        'messages' => ['reply']
    ], 'uses' => 'AdminController@replyToThread']);

    Route::post('/admin/users/{user}/edit', ['required' => [
        'users' => ['view', 'edit']
    ], 'uses' => 'AdminController@editUser']);

    Route::post('/admin/config', 'Admin\ConfigController@save');

    /*
     * Partials Requests
     */

    Route::get('/admin/partial/default_email', function ()
    {
        return view('partials.default_email');
    });
});
<?php

/*
 * All GET Routes for the Top-Level URI
 */

Route::get('/', 'PagesController@about');
Route::get('/about', 'PagesController@about');
Route::get('/blog', 'PostController@index');

//Make this route respond to many different extensions
//For example, JSON & XML perhaps...
Route::get('/blog/{post}.amp', 'PostController@showAMP');
Route::get('/blog/{post}', 'PostController@show');
Route::get('/tracks', 'PagesController@tracks');
Route::get('/videos', 'PagesController@videos');
Route::get('/contact', 'PagesController@contact');

Route::get('/admin', function ()
{
    return redirect('/admin/dashboard');
});
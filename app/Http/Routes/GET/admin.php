<?php

/*
 * All GET Routes for the /admin URI
 */

Route::group(['middleware' => ['auth', 'permissions']], function ()
{

    Route::get('/admin/dashboard', 'AdminController@dashboard');

    Route::get('/admin/posts', ['required' => [
        'posts' => ['view']
    ], 'uses' => 'AdminController@posts']);

    Route::get('/admin/messages', ['required' => [
        'messages' => ['view']
    ], 'uses'  => 'AdminController@messages']);

    Route::get('/admin/messages/{thread}', ['required' => [
        'messages' => ['view']
    ], 'uses' => 'AdminController@thread']);

    Route::get('/admin/users', ['required' => [
        'users' => ['view']
    ], 'uses'  => 'AdminController@users']);

    Route::get('/admin/users/{user}/edit', ['required' => [
        'users' => ['view', 'edit']
    ], 'uses' => 'AdminController@showUser']);

    Route::get('/admin/config', ['required' => [
        'config' => ['view']
    ], 'uses' => 'AdminController@config']);

});
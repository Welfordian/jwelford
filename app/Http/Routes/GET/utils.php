<?php

/*
*   All GET Request for utilities
*/

use Illuminate\Support\Facades\Input;

Route::get('/utils/js/analytics', function(){
    header('Content-type: text/javascript');
    echo file_get_contents("http://www.google-analytics.com/analytics.js");
});

Route::get('/utils/image/safe', function(Request $request){
    header('Content-Type: image/jpeg');
    echo file_get_contents(Input::get('url'));
});
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmailAddress;
use App\Http\Requests;

class ApiController extends Controller {

    /**
     * Return a list of emails which match the search query
     * of a given email address
     *
     * @param Request $request
     * @return mixed
     */

    public function searchEmails(Request $request)
    {
        return EmailAddress::searchFor($request->email)->limit(5)->get();
    }
}

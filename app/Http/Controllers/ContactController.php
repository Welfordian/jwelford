<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactMailRequest;
use Mail;

class ContactController extends Controller {

    /**
     * Send a contact email from the values the user
     * provides in the contact form
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function mail(ContactMailRequest $request)
    {
        Mail::send('emails.contact', ['mail' => $request->all()], function ($m) use ($request)
        {
            $m->from($request->email, $request->name);

            $m->to("josh@jwelford.co.uk", "Joshua Welford")->subject('New Message from Site');
        });

        $request->session()->flash('response', 'Thanks for contacting me, I\'ll get back to you as soon as possible');

        return back();
    }
}

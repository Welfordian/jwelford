<?php

namespace App\Http\Controllers;

use App\Config;
use App\Post;
use Welfordian\Models\Reply;
use Welfordian\Models\Thread;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Mail;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use Validator;
use View;
use DB;

class AdminController extends Controller {

    /**
     * AdminController constructor.
     */

    public function __construct(\Illuminate\Contracts\Auth\Factory $auth)
    {
        $this->auth = $auth;

        View::share([
            'nav_posts_count' => Post::count(),
            'nav_email_count' => Thread::where(['seen' => '0'])->count(),
            '_user'           => $auth->user(),
        ]);
    }

    /**
     * Return the view for /admin/dashboard
     *
     * @return View
     */

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    /**
     * Return the view for /admin/posts
     *
     * @return View
     */

    public function posts()
    {
        $posts = Post::orderBy('id', 'DESC')->get();

        return view('admin.posts', compact('posts'));
    }

    /**
     * Return the view for /admin/messages
     *
     * @return View
     */

    public function messages()
    {
        $threads = Thread::orderBy('id', 'DESC')->get();

        return view('admin.messages', [
            'thread_id' => false,
            'title'     => 'Messages',
            'threads'   => $threads,
        ]);
    }

    /**
     * Return the view for /admin/messages/{thread}
     *
     * @param Thread $thread
     * @return View
     */
    public function thread(Thread $thread)
    {
        $replies = $thread->replies()->get();
        $thread->seen();
        $messages = $thread->mails()->orderBy('id', 'DESC')->get();
        $replies = $thread->replies()->orderBy('id', 'DESC')->get();
        $messages = $messages->merge($replies)->sortByDesc('created_at');
        $threads = $thread->orderBy('id', 'DESC')->get();

        return view('admin.thread', [
            'thread_id' => $thread->id,
            'title'     => 'Conversation with ' . $thread->participant,
            'threads'   => $threads,
            'messages'  => $messages,
        ]);
    }

    /**
     * Handle a new reply on the given thread
     *
     * @param Request $request
     * @param Thread $thread
     * @return $this|\Illuminate\Http\RedirectResponse
     */

    public function replyToThread(Request $request, Thread $thread, \Pubnub\Pubnub $pubnub)
    {
        $reply_to = $thread->participant;

        $validator = Validator::make($request->all(), [
            'reply_body' => 'required|min:5',
        ]);

        if ($validator->fails())
        {
            $request->session()->flash('status', [
                'type'    => 'danger',
                'icon'    => 'exclamation-mark',
                'message' => 'Reply is required!',
            ]);

            return back()->withErrors($validator)->withInput();
        }

        $cssToInlineStyles = new CssToInlineStyles();
        $html = $request->reply_body;
        $css = file_get_contents(__DIR__ . '/../../../public/css/app.css');
        $reply_body = $cssToInlineStyles->convert($html, $css);

        Mail::send('emails.reply', ['reply_body' => $reply_body], function ($m) use ($request, $reply_to, $thread)
        {
            $m->from("josh@jwelford.co.uk", $this->auth->user()->name);

            $m->to($reply_to, "")->subject("RE: " . $thread->subject);
        });

        $reply = new Reply(['from' => 'internal_user_id(' . $this->auth->user()->id . ')', 'body-html' => $request->reply_body]);
        $thread->replies()->save($reply);

        $pubnub->publish('jwelford.new_email_reply', [
            'thread_id' => $thread->id,
            'by'        => $this->auth->user()->name,
            'action'    => 'new_reply',
        ]);

        return back();
    }

    /**
     * Return the view for /admin/users
     *
     * @return View
     */

    public function users()
    {
        $users = User::all();

        return view('admin.users', compact('users'));
    }

    /**
     * Return the view for /admin/config
     *
     * @param Config $config
     * @return View
     */

    public function config(Config $config)
    {
        $config = DB::select('SELECT * FROM site.configs LIMIT 1');
        $configs = json_decode($config[0]->default_permissions, true);
        return view('admin.config', compact('configs'));
    }

    /**
     * Return the view for /admin/user/{user}
     *
     * @param User $user
     * @return View
     */

    public function showUser(User $user)
    {
        $default_permissions = Config::defaultPermissions();

        return view('admin.user', compact('user', 'default_permissions'));
    }

    /**
     * Handle the editing of a user
     *
     * @param Request $request
     * @param User $user
     * @return $this|\Illuminate\Http\RedirectResponse
     */

    public function editUser(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required',
            'email'    => 'required|email',
            'password' => 'sometimes|confirmed|min:6',
        ]);

        if ($validator->fails())
        {
            $request->session()->flash('status', [
                'type'    => 'danger',
                'icon'    => 'exclamation-circle',
                'message' => 'Whoops! Looks like that didn\'t work. Check the erors below and try again.',
            ]);

            return back()->withErrors($validator)->withInput();
        }

        $permissions = (isset($request->permissions) && $request->permissions !== null) ? $request->permissions : "empty";

        if ($this->auth->user()->hasPermission('users', 'change permissions'))
        {
            $user->permissions = json_encode($this->parsePermissions($permissions));
        }
        $user->name = $request->name;
        $user->email = $request->email;

        if (isset($request->password) && $request->password !== "")
        {
            $user->password = bcrypt($request->password);
        }

        $user->update();

        $request->session()->flash('status', [
            'type'    => 'success',
            'icon'    => 'check',
            'message' => 'User information for ' . $user->Name . ' successfully saved',
        ]);

        return back();
    }

    /**
     * Remove the pesky single quotes from the permissions
     * and return an array ready for use
     *
     * @param $request_permissions
     * @return array
     */

    private function parsePermissions($request_permissions)
    {
        if ($request_permissions === "empty")
        {
            return [];
        }

        $permissions = [];

        foreach ($request_permissions as $master => $child)
        {
            $keys = array_keys($child);
            $real_keys = [];

            foreach ($keys as $key)
            {
                $real_keys[] = str_replace("'", "", $key);
            }

            $master = str_replace("'", "", $master);
            $permissions[ $master ] = $real_keys;
        }

        return $permissions;
    }
}

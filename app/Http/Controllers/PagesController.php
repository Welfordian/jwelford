<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;

class PagesController extends Controller {

    public function about()
    {
        return view('pages.about');
    }

    public function tracks()
    {
        return view('pages.tracks');
    }

    public function videos()
    {
        return view('pages.videos');
    }

    public function contact()
    {
        return view('pages.contact');
    }
}

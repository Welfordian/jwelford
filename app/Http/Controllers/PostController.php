<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Repositories\PostRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class PostController extends Controller {

    public function index(PostRepository $postRepository)
    {
        $posts = $postRepository->all();

        return view('pages.blog', compact('posts'));
    }

    public function showAMP(Request $request, $slug, PostRepository $postRepository)
    {
        $post = $postRepository->bySlug($slug);

        return view('blog.post_amp', compact('post'));
    }

    public function show(Request $request, $slug, PostRepository $postRepository)
    {
        $post = $postRepository->bySlug($slug);

        return view('blog.post', compact('post'));
    }
}

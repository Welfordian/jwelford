<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Welfordian\Models\Email;
use Welfordian\Models\Thread;
use App\EmailAddress;
use App\Http\Requests;
use Pubnub\Pubnub;

//This entire class needs to be cleaned up
//I'm really sorry about this...

class MailController extends Controller {

    public function inbound(Request $request, \Pubnub\Pubnub $pubnub)
    {
        if (! $this->valid_mailgun_request(config('app.MAILGUN_SECRET'), $request->token, $request->timestamp, $request->signature))
        {
            return response()->json([
                'mailgun_token_mismatch' => 'Token is invalid',
            ], 403);
        }

        $subject = (isset($request->subject) && $request->subject !== "null") ? $request->subject : "No subject";

        $re = ['RE: ', 'Re: ', 're: ', 'RE:', 'Re:', 're:'];
        
        $subject = str_replace($re, "", $subject);

        if ($thread = Thread::where(['participant' => $request->sender, 'subject' => $subject])->first())
        {
            $email = new Email($request->all());
            $thread->mails()->save($email);
            $pubnub->publish('jwelford.new_email_reply', [
                'thread_id' => $thread->id,
                'by'        => $request->sender,
                'action'    => 'new_reply',
            ]);
        } else
        {
            $thread = new Thread(['participant' => $request->sender, 'subject' => $subject]);
            $email = new Email($request->all());
            $thread->save();
            $thread->mails()->save($email);
            $pubnub->publish('jwelford.new_email_reply', [
                'thread_id' => $thread->id,
                'avatar'    => $thread->getAvatar(),
                'count'     => $thread->mails()->count(),
                'subject'   => $subject,
                'by'        => $request->sender,
                'action'    => 'new_thread',
            ]);
        }
        if (! count(EmailAddress::where(['address' => $request->sender])->get()))
        {
            (new EmailAddress(['address' => $request->sender]))->save();
        }

        return response()->json([
            'mailgun_inbound_acceptance' => 'OK',
        ], 200);
    }

    private function valid_mailgun_request($apiKey, $token, $timestamp, $signature)
    {
        //check if the timestamp is fresh
        if (time() - $timestamp > 15)
        {
            return false;
        }

        //returns true if signature is valid
        return hash_hmac('sha256', $timestamp . $token, $apiKey) === $signature;
    }
}

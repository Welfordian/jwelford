<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactMailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
      * Get the messages that apply to the request
      *
      * @return array
      */
    public function messages()
    {
        return [
            'name.required'    => 'I need your name so I know how to address you.',
            'email.required'   => 'I need your email address so I can get back to you.',
            'email.email'      => 'I need a valid email address so I can get back to you.',
            'subject.required' => 'Please provide a brief description in the subject line.',
            'message.required' => 'You need to enter a message.',

        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required',
            'email'   => 'required|email',
            'subject' => 'required',
            'message' => 'required',
        ];
    }
}

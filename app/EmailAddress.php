<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailAddress extends Model {

    protected $fillable = [
        'address',
    ];

    public function scopeSearchFor($query, $address)
    {
        if ($address)
            $query->where('address', 'LIKE', $address . '%');
    }
}

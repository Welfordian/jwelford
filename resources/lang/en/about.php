<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'titles' => [

        'summary' => 'Summary',

        'knowledge' => 'Knowledge of Languages',

        'experience' => 'Experience',

        'education' => 'Education',

        'interests' => 'Interests',
    
    ],

    'subtitles' => [

        'html' => 'HTML(5) - 4 Years Exp',

        'php' => 'PHP - 3 Years Exp',

        'js' => 'Javascript - 4 Years Exp',

        'css' => 'CSS - 4 Years Exp',

        'jquery' => 'jQuery - 3 Years Exp',

        'angular' => 'AngularJS - 1 Years Exp',

        'ajax' => 'AJAX & JSON - 2 Years Exp',

        'node' => 'NodeJS - > 1 Years Exp',

        'sass' => 'Sass - > 1 Years Exp',
    
    ],

    'texts' => [

        'summary' => 'I am a Web Developer who speciailizes in PHP, SQL, HTML & Javascript. I also have familiarity with .NET, Ruby & Python. but am looking to expand my knowledge in those areas. I have experience in creating CMS\' and mainly write in the OOP aspect. I have also used many REST API\'s and so, in turn, am frequently using JSON and XML. I am also getting quite familiar with AngularJS & SASS.',

        'html' => 'I believe knowledge of HTML is crucial in Web Development and would say my understanding of it is up to current standards.',
        'php' => 'I can easily create interactive and dynamic websites using PHP alongside MySQL(i). I prefer the OOP aspect of PHP.',

        'javascript' => 'I consider my knowledge of Javascript to be up to the current required standards of Web Development.',

        'css' => 'Not my favourite languages, but it\'s another one that\'s needed. I can easily and proficiently use CSS.',

        'jquery' => 'I like to have fun with jQuery. It gives such ease to manipulating DOM elements.',

        'angular' => 'AngularJS is a fantastic framework. It makes creating SPA\'s as easy as 1-2-3.',

        'ajax' => 'Ajax is another great one. Before Angular I would use Ajax to retrieve JSON & Fragments inside jQuery to create SPA\'s.',

        'node' => 'Perfect for builing real-time applications among other utilities such as minifying and package requirement. I enjoy using NodeJS.',

        'sass' => 'I have not used Sass a lot as I have only just started looking at it - from what I can see, it can save a lot of time when replicating certain CSS. It\'s clean, too!',

        'interests' => 'All types of music has always been an interest of mine. Though I don\'t play  an instrument, I\'m an avid listener. Programming, of course, is a large interest, which is why my chosen career is such a joy.',
    
    ],

    'experience' => [

        'better' => [
            'timescale' => 'Aug 2016 - Present (PHP Developer)',
            'description' => 'No description as of yet.'
        ],

        'gbizit' => [
            'timescale' => 'Sept 2013 - Jun 2016 (Web Developer - Gbiz IT Ltd / Marske-By-The-Sea)',
            'description' => 'Working with clients of differing needs, and being the only web developer at the company, I often need to leverage a wide variety of skills. The most robust project entailed creating a full job system from scratch for a tree surgeon company. This involved crafting a solution which enabled different users to have different access levels. Employees were not to see the whole list of clients and enquiries as they could move onto another job and take the list with them. The main languages involved were: PHP, SQL, HTML, JavaScript, AJAX and JSON.'
        ],

    ],

    'education' => [
        'rcc' => [
            'title' => 'Redcar & Cleveland College: BTEC Level 2 Diploma in I.T.',
            'quals' => [
                'hard_web' => 'Computer Hardware, Web Development & Event Driven Programming'
            ]
        ]
    ]

];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'titles' => [

        'summary' => 'Zusammenfassung',

        'knowledge' => 'Sprachkenntnisse'

    ],

    'subtitles' => [

        'html' => 'HTML(5) - 4 Jahre',

        'php' => 'PHP - 3 Jahre',

        'js' => 'Javascript - 4 Jahre',

        'css' => 'CSS - 4 Jahre',

        'jquery' => 'jQuery - 3 Jahre',

        'angular' => 'AngularJS - 1 Jahre',

        'ajax' => 'AJAX & JSON - 2 Jahre',

        'node' => 'NodeJS - > 1 Jahre',

        'sass' => 'Sass - > 1 Jahre'

    ],

    'texts' => [

         'summary' => 'Ich bin ein Web-Entwickler, die in PHP speciailizes, SQL, HTML & Javascript. Ich habe auch die Vertrautheit mit .NET, Ruby & Python. aber freue mich auf meine Kenntnisse in diesen Bereichen zu erweitern. Ich habe Erfahrung CMS bei der Schaffung von "und vor allem in der OOP Aspekt schreiben. Ich habe auch viele REST-API verwendet und so, die wiederum am häufigsten JSON und XML. Ich erhalte auch sehr vertraut mit AngularJS & SASS.',

         'html' => 'Ich glaube, dass HTML-Kenntnisse in Web-Entwicklung von entscheidender Bedeutung ist und würde sagen, mein Verständnis ist es an aktuellen Standards ist.',

         'php' => 'Ich kann einfach erstellen interaktive und dynamische Webseiten mit PHP neben MySQL (i). Ich ziehe den OOP Aspekt von PHP.',

         'javascript' => 'Ich halte mein Wissen von Javascript zu sein bis hin zu den aktuellen geforderten Standards der Web-Entwicklung.',

         'css' => 'Nicht mein Lieblings Sprachen, aber es ist eine andere, die benötigt wird. Ich kann einfach und proficiently CSS verwenden.',

         'jquery' => 'Ich mag Spaß mit jQuery zu haben. Es gibt eine solche Leichtigkeit DOM-Elemente zu manipulieren.',

         'angular' => 'AngularJS ist ein fantastischer Rahmen. Es macht SPAce so einfach wie 1-2-3 zu schaffen.',

         'ajax' => 'Ajax ist ein weiterer großer. Vor Angular würde ich Ajax verwenden JSON & Fragmente innerhalb jQuery abrufen SPAs zu erstellen.',

         'node' => 'Perfekt für den Aufbau von Echtzeitanwendungen unter anderem Dienstprogramme wie Verkleinerungs und Paketanforderung . Ich genieße NodeJS verwenden.',
         
         'sass' => 'Ich habe Sass nicht viel verwendet, da ich gerade erst begonnen, es zu betrachten - von dem, was ich sehen kann, kann es eine Menge Zeit sparen, wenn bestimmte CSS replizieren. Es ist sauber, auch!',

    ]

];

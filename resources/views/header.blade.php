@if( Session::get('language', 'en') !== "en")
    <div class="alert alert-warning" style="position: fixed; bottom: 8px; right: 24px; z-index: 999;">
        <p><i class="fa fa-exclamation-circle"></i> &nbsp; The language feature is currently under development and may not work as intended &nbsp;<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button></p>
    </div>
@endif

<nav style="border-radius: 0px; position: fixed; top: 0; left: 0; width: 100%; z-index: 1000;" class="navbar navbar-default noselect" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/" style="width: auto; text-align: center;">Joshua Welford</a></div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li {{ (Request::is('about') || Request::is('/') ? 'class=active' : '') }} >
                    <a href="/about"><i class="glyphicon glyphicon-user"></i> About &nbsp;</a></li>
                <li {{ (Request::is('blog') || Request::is('blog/*') ? 'class=active' : '') }} >
                    <a href="/blog"><i class="glyphicon glyphicon-bullhorn"></i> Blog &nbsp;</a></li>
                <li {{ (Request::is('videos') ? 'class=active' : '') }} >
                    <a href="/videos"><i class="glyphicon glyphicon-play"></i> Videos &nbsp;</a></li>
                <li {{ (Request::is('tracks') ? 'class=active' : '') }} >
                    <a href="/tracks"><i class="glyphicon glyphicon-music"></i> Music &nbsp;</a></li>
                <li {{ (Request::is('contact') ? 'class=active' : '') }} >
                    <a href="/contact"><i class="glyphicon glyphicon-inbox"></i> Contact &nbsp;</a></li>
            </ul>

            @if (Auth::check())
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> {{ Auth::user()->name }}
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/admin/dashboard"><i class="glyphicon glyphicon-tasks"></i> Dashboard</a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/logout"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-language"></i> {{ Session::get('language', 'en') }}
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ Request::path() }}?lang=en"><i class="fa fa-language"></i> &nbsp; en</a>
                            </li>
                            <li>
                                <a href="{{ Request::path() }}?lang=de"><i class="fa fa-language"></i> &nbsp; de</a>
                            </li>
                        </ul>
                    </li>
                </ul>

            @else

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/login"><i class="glyphicon glyphicon-log-in"></i> Login</a></li>
                    <li class="dropdown">
                        <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-language"></i> {{ Session::get('language', 'en') }}
                            <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ Request::path() }}?lang=en"><i class="fa fa-language"></i> &nbsp; en</a>
                            </li>
                            <li>
                                <a href="{{ Request::path() }}?lang=de"><i class="fa fa-language"></i> &nbsp; de</a>
                            </li>
                        </ul>
                    </li>
                </ul>

        @endif
        <!--<ul class="nav navbar-nav navbar-right">
                <li class="dropdown"> <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-collapse-down"></i> Projects <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a target="_blank" href="http://jwelford.co.uk/todo/"><i class="glyphicon glyphicon-log-out"></i> ToDo App</a> </li>
                    </ul>
                </li>
            </ul> -->
        </div>
    </div>
</nav>

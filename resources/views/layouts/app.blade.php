<!DOCTYPE html>
<html>
  <head>
      <meta name="theme-color" value="#2c3e50" />
      <link rel="manifest" href="manifest.json">
      <meta name="mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="application-name" content="JW">
      <meta property="fb:pages" content="123440801430482" />
      <link rel="apple-touch-icon" sizes="57x57" href="https://cdn.jwelford.co.uk/icons/apple-icon-57x57.png">
      <link rel="apple-touch-icon" sizes="60x60" href="https://cdn.jwelford.co.uk/icons/apple-icon-60x60.png">
      <link rel="apple-touch-icon" sizes="72x72" href="https://cdn.jwelford.co.uk/icons/apple-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="76x76" href="https://cdn.jwelford.co.uk/icons/apple-icon-76x76.png">
      <link rel="apple-touch-icon" sizes="114x114" href="https://cdn.jwelford.co.uk/icons/apple-icon-114x114.png">
      <link rel="apple-touch-icon" sizes="120x120" href="https://cdn.jwelford.co.uk/icons/apple-icon-120x120.png">
      <link rel="apple-touch-icon" sizes="144x144" href="https://cdn.jwelford.co.uk/icons/apple-icon-144x144.png">
      <link rel="apple-touch-icon" sizes="152x152" href="https://cdn.jwelford.co.uk/icons/apple-icon-152x152.png">
      <link rel="apple-touch-icon" sizes="180x180" href="https://cdn.jwelford.co.uk/icons/apple-icon-180x180.png">
      <link rel="icon" type="image/png" sizes="192x192" href="https://cdn.jwelford.co.uk/icons/android-icon-192x192.png">
      <link rel="icon" type="image/png" sizes="32x32" href="https://cdn.jwelford.co.uk/icons/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="96x96" href="https://cdn.jwelford.co.uk/icons/favicon-96x96.png">
      <link rel="icon" type="image/png" sizes="16x16" href="https://cdn.jwelford.co.uk/icons/favicon-16x16.png">
  @yield('links')
    <meta name="msapplication-TileImage" content="https://cdn.jwelford.co.uk/icons/ms-icon-144x144.png">
      <meta name="apple-mobile-web-app-title" content="JW">
      <meta name="msapplication-starturl" content="/">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="og:title" content="Joshua Welford" />
      <meta name="og:image" content="https://cdn.jwelford.co.uk/images/me.jpg" />
      <meta name="description" content="The Home of Joshua Welford" />
      <meta name="keywords" content="Joshua Welford, joshua welford, Josh Welford, josh welford, Redcar, redcar, web developer, Web Developer" />
      <meta name="og:description" content="The Home of Joshua Welford" />
      <meta name="robots" content="index,follow" />
      <meta name="DC.title" content="Joshua Welford" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <title>Joshua Welford</title>
      <style>
          #loader{position:fixed;width:100%;height:100%;background:#2c3e50;top:0;left:0;z-index:500000}.loader{left:0;top:33%;margin:60px auto;font-size:10px;position:relative;text-indent:-9999em;border-top:1.1em solid rgba(255,255,255,.2);border-right:1.1em solid rgba(255,255,255,.2);border-bottom:1.1em solid rgba(255,255,255,.2);border-left:1.1em solid #fff;-webkit-transform:translateZ(0);-ms-transform:translateZ(0);transform:translateZ(0);-webkit-animation:load8 1.1s infinite linear;animation:load8 1.1s infinite linear}.loader,.loader:after{border-radius:50%;width:10em;height:10em}@-webkit-keyframes load8{0%{-webkit-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes load8{0%{-webkit-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}
      </style>
      <style id="pagespeed">
        a{display:none;}
      </style>
  </head>

  <body>

  <div id="loader" data-turbolinks-eval="false">
      <div class="loader"></div>
  </div>

  @include('header')

  <div id="container" class="container">

      @yield('content')

  </div>

  @include('footer')

  <script src="https://cdn.firebase.com/js/client/2.4.2/firebase.js" async></script>

  <script src="https://cdn.jwelford.co.uk/js/app.js" async></script>

  <script>
    var loadDeferredStyles = function() {
      var styles = [
        '//cdn.jwelford.co.uk/css/font-awesome.min.css',
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        'https://cdn.jwelford.co.uk/css/app.css',
        @yield('css')
      ];
      var head = document.getElementsByTagName("head")[0];
      styles.forEach(function(sheet, i){
        var link = document.createElement("link");
        link.rel = "stylesheet";
        link.type = "text/css";
        link.href = sheet;
        if(i === styles.length -1){
          link.addEventListener('load', function(){
            document.getElementById("pagespeed").remove();
            document.getElementById('loader').remove();
          });
        }
        head.appendChild(link);
      });
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-81627014-1', 'auto');
    ga('send', 'pageview');

  </script>

  </body>

</html>
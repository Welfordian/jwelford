<!DOCTYPE html>

<html>

<head>

    <meta name="theme-color" value="#2c3e50" />

    <link rel="manifest" href="manifest.json">

    <meta name="mobile-web-app-capable" content="yes">

    <meta name="apple-mobile-web-app-capable" content="yes">

    <meta name="application-name" content="JW">

    <link rel="apple-touch-icon" sizes="57x57" href="/icons/apple-icon-57x57.png">

    <link rel="apple-touch-icon" sizes="60x60" href="/icons/apple-icon-60x60.png">

    <link rel="apple-touch-icon" sizes="72x72" href="/icons/apple-icon-72x72.png">

    <link rel="apple-touch-icon" sizes="76x76" href="/icons/apple-icon-76x76.png">

    <link rel="apple-touch-icon" sizes="114x114" href="/icons/apple-icon-114x114.png">

    <link rel="apple-touch-icon" sizes="120x120" href="/icons/apple-icon-120x120.png">

    <link rel="apple-touch-icon" sizes="144x144" href="/icons/apple-icon-144x144.png">

    <link rel="apple-touch-icon" sizes="152x152" href="/icons/apple-icon-152x152.png">

    <link rel="apple-touch-icon" sizes="180x180" href="/icons/apple-icon-180x180.png">

    <link rel="icon" type="image/png" sizes="192x192" href="/icons/android-icon-192x192.png">

    <link rel="icon" type="image/png" sizes="32x32" href="/icons/favicon-32x32.png">

    <link rel="icon" type="image/png" sizes="96x96" href="/icons/favicon-96x96.png">

    <link rel="icon" type="image/png" sizes="16x16" href="/icons/favicon-16x16.png">

    <meta name="msapplication-TileImage" content="/icons/ms-icon-144x144.png">

    <meta name="apple-mobile-web-app-title" content="JW">

    <meta name="msapplication-starturl" content="/">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="csrf_token" id="x-csrf-token" value="{{ csrf_token() }}" />

    <meta name="description" content="Home of Joshua Welford" />

    <meta name="keywords" content="Joshua Welford, joshua welford, Josh Welford, josh welford, Redcar, redcar, web developer, Web Developer" />

    <meta name="robots" content="index,follow" />

    <meta name="DC.title" content="Joshua Welford" />

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>JW - Admin</title>

    <link rel="stylesheet" href="//cdn.jwelford.co.uk/css/font-awesome.min.css" media="screen" charset="utf-8" defer async />

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" defer async />

    <link rel="stylesheet" href="/css/admin.css" defer async />

    <link rel="stylesheet" href="/css/pnotify.custom.min.css" defer async />

    <link rel="stylesheet" href="https://rawgit.com/flatlogic/awesome-bootstrap-checkbox/master/demo/build.css" defer async />

    @yield('css')

</head>

<body>

@include('admin.header')

<div id="container" class="container">

    @yield('content')

</div>

@include('admin.footer')

<script src="https://cdn.firebase.com/js/client/2.4.2/firebase.js"></script>

<script src="/js/admin.js"></script>

</body>

</html>

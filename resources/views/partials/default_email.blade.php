<div id="mailsub" class="notification" align="center">
    <table style="min-width: 320px;" border="0" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td align="center" bgcolor="#eff3f8"><!-- [if gte mso 10]>
                <table width="680" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td> <![endif]-->
                <table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody>
                    <tr>
                        <td><!-- padding -->
                            <div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
                        </td>
                    </tr> <!--header -->
                    <tr>
                        <td align="center" bgcolor="#ffffff"><!-- padding -->
                            <div style="height: 30px; line-height: 30px; font-size: 10px;">&nbsp;</div>
                            <table border="0" width="90%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td align="center">
                                        <a href="https://jwelford.co.uk" target="_blank"><h2>{{ Auth::user()->name }}</h2></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table> <!-- padding -->
                            <div style="height: 5px; line-height: 50px; font-size: 10px;">&nbsp;</div>
                        </td>
                    </tr> <!--header END--> <!--content 1 -->
                    <tr>
                        <td align="center" bgcolor="#fbfcfd">
                            <table border="0" width="90%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td align="center"><!-- padding -->
                                        <div style="height: 60px; line-height: 60px; font-size: 10px;">&nbsp;</div>
                                        <div style="line-height: 44px;">
                                            <span style="font-size: x-large; color: #57697e; font-family: Arial, Helvetica, sans-serif;"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">Hi there, {name}</span></span>
                                        </div> <!-- padding -->
                                        <div style="height: 40px; line-height: 40px; font-size: 10px;">&nbsp;</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <div style="line-height: 24px;">
                                            <span style="font-size: large; color: #57697e; font-family: Arial, Helvetica, sans-serif;"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{some awesome text about me seeing you}</span></span>
                                        </div> <!-- padding -->
                                        <div style="height: 40px; line-height: 40px; font-size: 10px;">&nbsp;</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <div style="line-height: 24px;">Thanks, {{ Auth::user()->name }}.</div> <!-- padding -->
                                        <div style="height: 60px; line-height: 60px; font-size: 10px;">&nbsp;</div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr> <!--content 1 END--> <!--content 2 --> <!--links -->
                    <tr>
                        <td style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;" align="center" bgcolor="#ffffff">
                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td align="center"><!-- padding -->
                                        <div style="height: 32px; line-height: 32px; font-size: 10px;">&nbsp;</div>
                                        <span style="color: #5b9bd1; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 22px;">{{ date("F jS, Y h:i A") }}</span></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><!-- padding -->
                                        <div style="height: 32px; line-height: 32px; font-size: 10px;">&nbsp;</div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr> <!--footer END-->
                    <tr>
                        <td><!-- padding -->
                            <div style="height: 80px; line-height: 80px; font-size: 10px;">&nbsp;</div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                                                 <!-- [if gte mso 10]> </td></tr> </table> <![endif]--></td>
        </tr>
        </tbody>
    </table>
</div>
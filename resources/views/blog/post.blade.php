@extends('layouts.app')

@section('links')
    <link rel="amphtml" href="https://jwelford.co.uk/blog/{{ $post->slug }}.amp">
@stop

@section('content')

    <meta property="og:title" content="{{ $post->title }}" />
    <meta property="og:url" content="https://jwelford.co.uk/blog/{{ $post->id }}" />
    <meta property="og:image" content="{{ $post->imageUrl }}" />
    <p style="font-size: 18px;" class="noselect">
        <a href="/blog"><i class="glyphicon glyphicon-circle-arrow-left"></i>
            All Posts</a>
        <a class="pull-right" href="https://jwelford.co.uk/blog/{{ $post->slug }}.amp"><img style="width: 24px;" src="https://jwelford.co.uk/images/amp.PNG" /></a>
    </p>
    <h1>{{ $post->title }}</h1>
    <p>{!! $post->body !!}</p>

    <div id="disqus_thread" style="margin-top: 25px;"></div>
    <script>
        /**
         * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
         */
        /*
         var disqus_config = function () {
         this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
         this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
         };
         */
        (function () { // DON'T EDIT BELOW THIS LINE
            var d = document,
                    s = d.createElement('script');

            s.src = '//joshwelford.disqus.com/embed.js';

            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>

@stop

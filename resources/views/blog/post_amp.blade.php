<!doctype html>
<html amp lang="en">
  <head>
    <meta charset="utf-8">
    <title>JW | {{ $post->title }}</title>
    <link rel="canonical" href="https://jwelford.co.uk/blog/{{ $post->slug }}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic" />
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "NewsArticle",
        "headline": "{{ $post->title }}",
        "mainEntityOfPage": "https://jwelford.co.uk/blog/{{ $post->slug }}",
        "author": "Joshua Welford",
        "publisher": {
          "@type": "Organization",
          "name": "Joshua Welford",
          "logo": {
            "@type": "ImageObject",
            "url": "https://jwelford.co.uk/icons/favicon-96x96.png",
            "width": 238,
            "height": 60
          }
        },
        "datePublished": "{{ $post->created_at->toW3cString() }}",
        "dateModified": "{{ $post->updated_at->toW3cString() }}",
        "image": {
          "@type": "ImageObject",
          "url": "{{ $post->imageUrl }}",
          "height": 549,
          "width": 976
        }
      }
    </script>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <style amp-custom>
    .container {
      font-family: 'Questrial', Arial;
      margin: 0 auto 42px;
      max-width: 600px;
      color: #2c3e50;
      font-family:Lato,Helvetica Neue,Helvetica,Arial,sans-serif;
      font-size: 14px;
      padding: 18px;
      
    }
    article {
      display: block;
      max-width: 736px;
      margin: 8px;
    }
    .bordered {
      border: 1px solid black;
    }
    .lightbox1 {
      background: #222;
    }
    .lightbox1-content {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      display: flex;
      flex-direction: column;
      flex-wrap: nowrap;
      justify-content: center;
      align-items: center;
    }
    .lightbox1 amp-img {
      max-width: 80%;
      max-height: 80%;
      width: 100%;
    }
    .box1 {
      border: 1px solid black;
      margin: 8px 0;
    }
    .logo {
      background: yellow;
      border: 5px solid black;
      border-radius: 50%;
      opacity: 0.7;
      position: fixed;
      z-index: 1000;
      top: 100px;
      right: 10px;
      width: 40px;
      height: 40px;
    }
    @media screen and (min-width: 380px) {
      .logo {
        top: auto;
      }
    }
    @media screen and (min-width: 420px) {
      .logo {
        position: static;
        top: auto;
      }
    }
    .goto {
      display: block;
      margin: 16px;
    }
    @font-face {
      font-family: 'Comic AMP';
      font-style: bold;
      font-weight: 700;
      src: url(fonts/ComicAMP.ttf) format('truetype');
    }
    .comic-amp-font-loaded .comic-amp {
      font-family: 'Comic AMP', serif, sans-serif;
    }
    .comic-amp-font-loading .comic-amp {
      color: #0ff;
    }
    .comic-amp-font-missing .comic-amp {
      color: #f00;
    }
    #amp-iframe:target {
      border: 1px solid green;
    }
    #breaker {
      height: 100px;
      background: green;
    }
    #breaking {
      height: 200px;
      width: 200vw;
      background: rgba(0, 0, 0, 0.5);
    }
    .responsive-image {
        height: 0;
        overflow: hidden;
        padding-bottom: 56.25%;
        transition: opacity 0.5s ease-in 0s;
        -webkit-animation: fadein 2s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 2s; /* Firefox < 16 */
        -ms-animation: fadein 2s; /* Internet Explorer */
         -o-animation: fadein 2s; /* Opera < 12.1 */
            animation: fadein 2s;
      }
    .headline {
        color: #2c3e50;
        font-size: 32px;
        font-weight: bold;
        line-height: 1.125;
        margin-bottom: 0;
        text-align: left;
        letter-spacing: -0.5px;
      }
      .author{
        font-weight: bold;
      }
      p{
        font-size: 16px;
        line-height: 22px;
        text-align: justify;
      }
      .site-brand{
        margin: -18px 0 18px 0;
        padding: 2px;
        background: #2c3e50;
        text-align: center;
      }
      .site-brand a:visited, .site-brand a:active, .site-brand a{
        color: #ffffff;
        text-decoration: none;
      }
      li{
        margin: 8px;
      }
      @keyframes fadein {
        from { opacity: 0; }
        to   { opacity: 1; }
    }

    /* Firefox < 16 */
    @-moz-keyframes fadein {
        from { opacity: 0; }
        to   { opacity: 1; }
    }

    /* Safari, Chrome and Opera > 12.1 */
    @-webkit-keyframes fadein {
        from { opacity: 0; }
        to   { opacity: 1; }
    }

    /* Opera < 12.1 */
    @-o-keyframes fadein {
        from { opacity: 0; }
        to   { opacity: 1; }
    }
  </style>
  </head>
  <body>
    <div class="container">
      <div class="site-brand">
      <h1><a href="https://jwelford.co.uk/">Joshua Welford</a></h1>
    </div>
    <h1 class="headline">{{ $post->title }}</h1>
    <p><span  class="author">Joshua Welford</span> | Last Updated {{ $post->updated_at->diffForHumans() }}</p>
    <amp-img src="{{ $post->imageUrl }}" class="responsive-image" alt="Welcome" width="976" height="549" layout="responsive"></amp-img>
    <p>
        {!! $post->body !!}
    </p>
    </div>  
  </body>

</html>
@extends('layouts.app')



@section('css')

    '/css/contact.css',
    '/css/bootstrap-social.css'

@stop

@section('content')

    <h1><i style="margin-left: 8px;" class="fa fa-envelope"></i> Get in touch with me</h1>
    @if( session('response') )
        <div class="row">
            <div class="col-md-9">
                <div class="alert alert-success">
                    <p style="text-align: center;">{{ session('response') }}</p>
                </div>
            </div>
        </div>
    @endif

    <div class="row">

        <div class="col-md-9">

            <form role="form" method="POST">

                <div class="input-group"><span ng-class="{'error': errors.name !==undefined}" class="input-group-addon">Full Name </span>

                    <input name="name" id="prependedtext" name="prependedtext" class="form-control ng-pristine ng-untouched ng-valid" placeholder="John Doe..." type="text" required value="{{ old('name') }}">

                </div>

                <div class="input-group"><span ng-class="{'error': errors.email !==undefined}" class="input-group-addon">Email Address </span>

                    <input name="email" id="prependedtext" name="prependedtext" class="form-control ng-pristine ng-untouched ng-valid" placeholder="john.doe@example.com" type="email" required value="{{ old('email') }}">

                </div>

                <div class="input-group"><span ng-class="{'error': errors.subject !==undefined}" class="input-group-addon">Subject </span>

                    <input name="subject" id="prependedtext" name="prependedtext" class="form-control ng-pristine ng-untouched ng-valid" placeholder="RE: Your Website..." type="text" required value="{{ old('subject') }}">

                </div>

                <div class="input-group"><span ng-class="{'error': errors.message !==undefined}" class="input-group-addon">Message </span>

                    <textarea name="message" style="resize: none;" class="form-control" placeholder="Your message here..." required>{{ old('message') }}</textarea>

                </div>

                <div class="col-md-6" style="padding: 5px;">

                    <button style="width: 100%;" class="btn btn-success">Send Message</button>

                </div>

                <div style="padding: 5px;" class="col-md-6">
                    <a ng-click="clearFormFields()" style="width: 100%;" class="btn btn-danger">Clear Form</a>

                </div>


                {{ csrf_field() }}

            </form>

        </div>

        <div class="col-md-3">
            <a href="https://twitter.com/welfordian" target="_blank" style="margin-top: 7px;" class="btn btn-block btn-social btn-twitter"><i style="margin-top: 3px;" class="fa fa-twitter"></i>
                Find me on
                Twitter</a><a href="https://github.com/welfordian" target="_blank" style="margin-top: 7px;" class="btn btn-block btn-social btn-github"><i style="margin-top: 3px;" class="fa fa-github"></i>
                Find me on GitHub</a>

        </div>

    </div>



    <div class="modal fade" tabindex="-1" role="dialog" id="errorsModal">

        <div class="modal-dialog">

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>

                    <h4 class="modal-title">Whoops, looks like you missed something...</h4>

                </div>

                <div class="modal-body">

                    <h4 style="margin-top: 0px; margin-bottom: 1em;">Address the errors below and try again.</h4>

                    <ol>

                        @foreach($errors->all() as $error)

                            <li>{{ $error }}</li>

                        @endforeach

                    </ol>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-primary" data-dismiss="modal">Okay, let me retry</button>

                </div>

            </div><!-- /.modal-content -->

        </div><!-- /.modal-dialog -->

    </div><!-- /.modal -->



    @if(count($errors))

        <script type="text/javascript">

            window.contactErrors = true;

        </script>

    @endif

@stop


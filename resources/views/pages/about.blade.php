@extends('layouts.app')

@section('content')


    <div class="row noselect" style="width: 100%; margin: 0px auto; background: url(//cdn.jwelford.co.uk/images/cover.jpg); background-position: center center; background-size: cover; position: relative;">
        <div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(255, 255, 255, 0.46);"></div>
        <div class="col-md-12">
            <img style="display: block; margin: 20px auto; width: 150px; height: 150px; box-shadow: 0px 0px 10px; background: url(//cdn.jwelford.co.uk/images/me.jpg) center center no-repeat; background-size: cover;" />
            <h2 style="text-align: center; color: black;">Joshua Welford</h2>
        </div>
    </div>
    <div class="row" id="summary"><a name="summary"></a>
        <div class="col-md-12">
            <h2 class="noselect"><span>@lang('about.titles.summary')</span>
                <a href="#summary" class="part-link"><i class="glyphicon glyphicon-link"></i></a></h2>
            <p style="text-align: justify;">@lang('about.texts.summary')</p>
        </div>
    </div>
    <div class="row" id="knowledge"><a name="knowledge"></a>
        <h2 class="noselect" style="margin-left: 15px;">@lang('about.titles.knowledge')
            <a href="#knowledge" class="part-link"><i class="glyphicon glyphicon-link"></i></a></h2>
        <div class="col-md-4">
            <h4 style="font-style: italic; text-decoration: underline;">@lang('about.subtitles.html')</h4>
            <p style="text-align: justify;">@lang('about.texts.html')</p>
        </div>
        <div class="col-md-4">
            <h4 style="font-style: italic; text-decoration: underline;">@lang('about.subtitles.php')</h4>
            <p style="text-align: justify;">@lang('about.texts.php')</p>
        </div>
        <div class="col-md-4">
            <h4 style="font-style: italic; text-decoration: underline;">@lang('about.subtitles.js')</h4>
            <p style="text-align: justify;">@lang('about.texts.javascript')</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h4 style="font-style: italic; text-decoration: underline;">@lang('about.subtitles.css')</h4>
            <p style="text-align: justify;">@lang('about.texts.css')</p>
        </div>
        <div class="col-md-4">
            <h4 style="font-style: italic; text-decoration: underline;">@lang('about.subtitles.jquery')</h4>
            <p style="text-align: justify;">@lang('about.texts.jquery')</p>
        </div>
        <div class="col-md-4">
            <h4 style="font-style: italic; text-decoration: underline;">@lang('about.subtitles.angular')</h4>
            <p style="text-align: justify;">@lang('about.texts.angular')</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h4 style="font-style: italic; text-decoration: underline;">@lang('about.subtitles.ajax')</h4>
            <p style="text-align: justify;">@lang('about.texts.ajax')</p>
        </div>
        <div class="col-md-4">
            <h4 style="font-style: italic; text-decoration: underline;">@lang('about.subtitles.node')</h4>
            <p style="text-align: justify;">@lang('about.texts.node')</p>
        </div>
        <div class="col-md-4">
            <h4 style="font-style: italic; text-decoration: underline;">@lang('about.subtitles.sass')</h4>
            <p style="text-align: justify;">@lang('about.texts.sass')</p>
        </div>
    </div>
    <div class="row" id="experience"><a name="experience"></a>
        <div class="col-md-12">
            <h2 class="noselect">@lang('about.titles.experience')
                <a href="#experience" class="part-link"><i class="glyphicon glyphicon-link"></i></a></h2>
            <p style="font-weight: 600;">@lang('about.experience.better.timescale')</p>
            <p style="text-align: justify;">@lang('about.experience.better.description')</p>
            <hr />
            <p style="font-weight: 600;">@lang('about.experience.gbizit.timescale')</p>
            <p style="text-align: justify;">@lang('about.experience.gbizit.description')</p>
        </div>
    </div>
    <div class="row" id="education"><a name="education"></a>
        <div class="col-md-12">
            <h2 class="noselect">@lang('about.titles.education')
                <a href="#education" class="part-link"><i class="glyphicon glyphicon-link"></i></a></h2>
            <p style="text-align: justify;">@lang('about.education.rcc.title')</p>
            <p style="text-align: justify;">@lang('about.education.rcc.quals.hard_web')</p>
        </div>
    </div>
    <div class="row" id="interests"><a name="interests"></a>
        <div class="col-md-12">
            <h2 class="noselect">@lang('about.titles.interests')
                <a href="#interests" class="part-link"><i class="glyphicon glyphicon-link"></i></a></h2>
            <p style="text-align: justify;">@lang('about.texts.interests')</p>
        </div>
    </div>
    <div style="width: 100%; height: 40px;"></div>

@stop

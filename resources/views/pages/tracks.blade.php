@extends('layouts.app')

@section('css')

    '/css/tracks.css'

@stop

@section('content')

    <h1 id="tracks-title"><i title="via Spotify" class="fa fa-spotify"></i> What I'm listening to these days...
        <i class="fa fa-refresh" @click="getTracks()"></i></h1>
    <hr />
    <h1 style="text-align: center; font-size: 6em; color: #2c3e50;" v-if="!tracks">
        <i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></h1>
    <div id="tracks-container" class="row v-cloak">
        <div class="col-md-3" v-for="track in tracks">
            <a class="tutorial-link" target="_blank" rel="noreferrer noopener" href="@{{ track.url }}">
                <div class="well well-custom tutorial">
                    <h4 class="title" id="title"><span title="@{{ track.name }}">@{{ track.name }}
                            - @{{ track.artist['#text'] }}</span></h4>
                    <div class="intro-image-container">
                        <img class="tutorial-intro-image image" id="track-image" src="https://jwelford.co.uk/lastFM-Image.php?lfm_url=@{{ track.image }}">
                    </div>
                </div>
            </a>
        </div>
    </div>

@stop

@extends('layouts.app')

@section('css')

    '/css/videos.css'

@stop

@section('content')

    <h1><i class="fa fa-youtube-play" style="color: #e52d27"></i> What I've been watching lately...</h1>
    <hr />
    <h1 style="text-align: center; font-size: 6em; color: #2c3e50;" v-if="!videos">
        <i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i></h1>
    <div class="row viewing-habits v-cloak">
        <div class="col-md-3 col-lg-3" v-for="video in videos">
            <a class="tutorial-link" target="_blank" rel="noreferrer noopener" href="@{{ video.url }}">
                <div class="well well-custom tutorial">
                    <h4 class="title" id="title"><span title="@{{ video.title }}">@{{ video.title }}</span></h4>
                    <div class="intro-image-container" style="position: relative;">
                        <img class="tutorial-intro-image image" v-bind:class="{'youtube': video.tag === 'youtube', 'netflix': video.tag === 'netflix'}" id="track-image" src="@{{ video.image }}">
                        <img class="intro-image-origin" v-if="video.tag == 'youtube'" src="//cdn.jwelford.co.uk/images/youtube-play.svg">
                        <img class="intro-image-origin" v-if="video.tag == 'netflix'" src="//cdn.jwelford.co.uk/images/netflix-n.svg" style="width: 145px;" />
                    </div>
                </div>
            </a>
        </div>
    </div>
@stop

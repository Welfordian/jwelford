@extends('layouts.app')

@section('content')


    @foreach ( $posts as $post )

        <div class="col-sm-4 col-md-4 post">
            <a class="post-link" href="/blog/{{ $post->slug }}">
                <div class="post-img-content">
                    <img src="{{ $post->imageUrl }}" class="img-responsive">
                    <span class="post-title"><b>{{ $post->title }}</b></span>
                </div>
            </a>
            <div class="content">
                <a class="post-link" href="/blog/{{ $post->slug }}">
                    <div style="text-align: center;" class="author">By <b>Josh W</b> | April 4th 2016</div>
                    <div style="text-align: justify;">{{ strip_tags(mb_strimwidth($post->body, 0, 300, "...")) }}</div>
                </a>
                <div style="margin-top: 12px;">
                    <a style="width: 100%;" href="/blog/{{ $post->slug }}" class="btn btn-primary btn-sm">Read more</a>
                </div>
            </div>
        </div>

    @endforeach


@stop

@extends('layouts.admin')

@section('content')

    <h1>Configuration</h1>
	
	<div class="indent" style="margin-left: 35px;">
		<h2>Permissions</h2>

		<!-- Remove inline styles... please. -->

	    <div class="indent" style="margin-left: 35px;">
			@foreach($configs as $type => $config)
		
				<h3>{{ ucwords($type) }}</h3>

				<ul>

				@foreach($config as $cfg)
					
					<li>{{ ucwords($cfg) }}</li>

				@endforeach

				<li style="list-style-type: none;"><span class="add_new_permission"><a href="#">Create New</a></span></li>
				
				</ul>

		    @endforeach
	    </div>
	</div>

	<script type="text/javascript">
		(function(){

			var startWithJquery = function()
			{
				var $ = jQuery,
					savingPermission = false;

				$.ajaxSetup({
		            data: {
		                _token: $('#x-csrf-token').attr("value")
		            }
		        });

				var addNewPermission = function(elem, oldHTML){

					if(elem.val().length !== 0 && !savingPermission)
					{

						var newPermissionValue = elem.val();
						savingPermission = true;

						$.ajax({
							method: "POST",
							data: { 'fucking': 'work' }
						}).done(function(response){
							elem.parent().before('<li>' + newPermissionValue + '</li>');
							elem.parent().html(oldHTML);
							savingPermission = false;
						}).fail(function(err){
							savingPermission = false;
						});
					}

				}

				$(document).on('click', '.add_new_permission', function(e){
					e.preventDefault();
					var parent = $(this).parent();
					var originalHTML = $(this).parent().html();
					var newInput = $('<input />').attr({ type: "text", placeholder: "New permission..." });
					$(this).parent().html(newInput);
					newInput.focus();
					newInput.on('blur', function(){
						addNewPermission(newInput, originalHTML);
					});

					newInput.on('keyup', function(e){
						if(e.which == 13){
							addNewPermission(newInput, originalHTML);
						}
					});

				});
			}

			var waitForJquery = setInterval(function()
			{
				if('jQuery' in window)
				{
					startWithJquery();
					clearInterval(waitForJquery);
				}
			}, 500);

		})();
	</script>

@stop
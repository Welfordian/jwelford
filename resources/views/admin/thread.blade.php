@extends('admin.messages')

@section('css')
    <style>
        .ui-pnotify-history-container {
            display: none !important;
        }
    </style>
@stop

@section('messages')

    <div class="list-group">
        @foreach( $messages as $message )
            @if (substr($message->from,0,16) == "internal_user_id")
                <div class="list-group-item clearfix">
                    <p style="margin-bottom: 16px; padding-bottom: 12px; font-weight: bold; border-bottom: 1px solid #ecf0f1;">{{ $message->parseInternalUser() }}
                        Replied <span class="pull-right">{{ $message->created_at->diffForHumans() }}</span></p>
                    <p>{!! $message['body-html'] !!}</p>
                </div>
            @else
                <div class="list-group-item clearfix">
                    <p style="margin-bottom: 16px; padding-bottom: 12px; font-weight: bold; border-bottom: 1px solid #ecf0f1;">{{ $message->from }}
                        <span class="pull-right">{{ $message->created_at->diffForHumans() }}</span></p>
                    <p>{!! $message['body-html'] !!}</p>
                </div>
            @endif


        @endforeach
        <div class="list-group-item clearfix">
            <form method="POST">
                <textarea class="richEmail form-control" name="reply_body"></textarea>
                <button class="btn btn-danger" style="margin-top: 12px;">Clear Reply</button>
                {{ csrf_field()  }}
                <button class="btn btn-success pull-right" style="margin-top: 12px;">Send Reply</button>
            </form>
        </div>
    </div>

@stop
@extends('layouts.admin')

@section('css')
    <style>

    </style>
@stop

@section('content')
    <h1 style="margin-bottom: 24px;">
        {{ $title }}
        @if (Request::is('admin/messages/*') && $_user->hasPermission('messages', 'delete'))
            <div class="dropdown pull-right" style="margin-top: -4px; margin-left: 12px;">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-cog" aria-hidden="true" style="margin-right: 8px;"></i> <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a style="color: #d62c1a;" href="#delete">Delete Thread</a></li>
                    <!--<li role="separator" class="divider"></li>-->
                </ul>
            </div>
        @endif
        @if( $_user->hasPermission('messages', 'create'))
            <button class="pull-right btn btn-success" data-toggle="modal" data-target="#newMessageModal">New Message
            </button>
        @endif
    </h1>

    <div class="row">
        <div class="col-md-3">
            <div class="list-group" style="max-height: 525px; overflow: auto;" id="thread_container">
                @foreach($threads as $thread)
                    <a href="/admin/messages/{{ $thread->id }}">
                        @if ( $thread_id == $thread->id )
                            <div class="list-group-item hover active">
                                @else
                                    <div class="list-group-item hover">
                                        @endif
                                        <div class="row">
                                            <div class="col-md-3">
                                                <img v-on:mousedown="@toggleThread" src="{{ $thread->getAvatar() }}" />
                                            </div>
                                            <div class="col-md-7">
                                                <p>{{ $thread->subject }}</p>
                                            </div>
                                            <div class-
                                            "col-md-2">
                                            @if (!$thread->seen)
                                                <p class="badge badge-success">{{ $thread->mails()->count() }}</p>
                                            @else
                                                <p class="badge">{{ $thread->mails()->count() }}</p>
                                            @endif
                                        </div>
                                    </div>
                            </div>
                    </a>
                @endforeach
            </div>
        </div>
        <div class="col-md-9">
            @yield('messages')
        </div>
    </div>

    <div class="modal fade" id="newMessageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">New Message</h4>
                </div>
                <div class="modal-body">
                    <div class="input-group" style="margin-bottom: 12px;">
                        <span class="input-group-addon">Email Address </span>
                        <div style="position: relative;">
                            <div class="list-group" v-if="typeahead.emailCollection" style="position: absolute; top: 45px; width: 100%; z-index: 10;">
                                <div v-on:mousedown="newEmail.emailAddress = email.address" class="list-group-item hover" v-for="email in typeahead.emailCollection">
                                    <p>@{{ email.address }}</p>
                                </div>
                            </div>
                            <textarea name="email" style="height: 45px; resize: none;" id="prependedtext" class="form-control" placeholder="john.doe@example.com" type="email" required="" value="" autcomplete="off" v-model="newEmail.emailAddress" v-on:keyup="emailAddressTypeahead()" v-on:blur="typeahead.emailCollection = false;"></textarea>
                        </div>
                    </div>
                    <div class="input-group" style="margin-bottom: 12px;">
                        <span class="input-group-addon">Subject </span>
                        <input class="form-control" type="text" placeholder="Subject" />
                    </div>
                    <textarea class="richEmail" style="min-height: 250px;" class="form-control" placeholder="Type something special here..."></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Send Message</button>
                </div>
            </div>
        </div>
    </div>

    <style>
        .badge-success {
            background-color: #5cb85c !important;
        }

        a .list-group-item {
            color: #2c3e50 !important;
        }

        .list-group-item.hover:hover {
            background-color: #2c3e50 !important;
            color: #ffffff !important;
            -webkit-transition: all 500ms;
            cursor: pointer;
        }

        .list-group-item.hover:hover .badge {
            background-color: #ffffff;
            color: #2c3e50;
            -webkit-transition: all 500ms;
        }

        .list-group-item.active {
            background-color: #2c3e50 !important;
            color: #ffffff !important;
            -webkit-transition: all 500ms;
        }

        .list-group-item.active .badge {
            background-color: #ffffff;
            color: #2c3e50;
            -webkit-transition: all 500ms;
        }

        .input-group-addon {
            min-width: 130px;
            text-align: right;
        }

        .mce-path {
            display: none !important;
        }

        .dropdown-menu li:hover a {
            color: #ffffff !important;
        }
    </style>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="https://cdn.pubnub.com/pubnub-3.15.2.js" type="text/javascript"></script>
    <script type="text/javascript">
        'use strict';
        var thread_id = /[^/]*$/.exec(window.location.href)[0];
        var pubnub = PUBNUB({
            publish_key: 'pub-c-e83816fe-a78f-4fe5-828b-d5bb83249219',
            subscribe_key: 'sub-c-8e0178a2-5180-11e6-9baf-0619f8945a4f',
            ssl: true
        });
        pubnub.subscribe({
            channel: "jwelford.new_email_reply",
            message: function (message, envelope, channelOrGroup, time, channel) {
                console.log(message);
                if ('action' in message) {
                    if (message['action'] === "new_reply") {
                        if (message['thread_id'] === parseInt(thread_id)) {
                            jQuery.pnotify({
                                title: 'New Reply',
                                text: message['by'] + ' just posted a new reply to this thread.',
                                type: 'info',
                                hide: false
                            });
                        }
                    }

                    if (message['action'] === "new_thread") {
                        var new_message = `<a href="/admin/messages/${message['thread_id']}" style="display: block;" > <div class="list-group-item hover" > <div class="row" > <div class="col-md-3" ><img src="${message['avatar']}" ></div > <div class="col-md-7" > <p >${message['subject']}</p > </div > <div class- "col-md-2"=""> <p class="badge badge-success" >${message['count']}</p > </div > </div > </div> </a >`;
                        var elem = jQuery(new_message.replace(/(\r\n|\n|\r)/gm, ""));
                        elem.attr("id", "newDivTHREAD").hide().appendTo("body");
                        var height = jQuery('#newDivTHREAD').outerHeight();
                        jQuery('#newDivTHREAD').remove();
                        elem.find(".list-group-item").css({"height": "0px", "transition": "=height 1s"});
                        elem.prependTo(jQuery('#thread_container')).slideDown();
                        elem.find(".list-group-item").css("height", height);
                    }
                }
            }
        });
    </script>
    <script>
        tinymce.init({
            selector: '.richEmail',
            theme_url: "/tinymce/modern/",
            skin_url: '/tinymce/light',
            height: 630,
            content_css: '/css/app.css',
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,
            setup: function (ed) {
                ed.on('init', function (args) {
                    jQuery.get('/admin/partial/default_email', function (data) {
                        ed.setContent(data);
                    });
                });
            }
        });
    </script>
@stop
@extends('layouts.admin')

@section('content')

    <h1>
        Users
        @if ($_user->hasPermission('users', 'create') )
            <button class="btn btn-success pull-right">Create User</button>
        @endif
    </h1>

    <table class="table table-hover">
        <thead>
        <th>Name</th>
        <th>Email Address</th>
        <th>Edit</th>
        <th>Delete</th>
        </thead>
        <tbody>
        @foreach ($users as $user)
            <tr>
                <td style="vertical-align: middle;">{{ $user->name }}</td>
                <td style="vertical-align: middle;">{{ $user->email }}</td>
                <td><a href="/admin/users/{{ $user->id }}/edit">
                        <button class="btn btn-info btn-sm"><i class="glyphicon glyphicon-pencil"></i></button>
                    </a>
                <td><a href="#">
                        <button class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-remove"></i></button>
                    </a></td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop
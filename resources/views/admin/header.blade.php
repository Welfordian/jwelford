@inject('_user', 'App\User')

<nav style="border-radius: 0px; position: fixed; top: 0; left: 0; width: 100%; z-index: 15;" class="navbar navbar-default noselect" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/dashboard" style="width: auto; text-align: center;">JW - Admin</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                @if (Request::user()->hasPermissions([
                    'posts' => ['view']
                ]))
                    <li {{ (Request::is('admin/posts') ? 'class=active' : '') }} >
                        <a href="/admin/posts"><i class="glyphicon glyphicon-bullhorn"></i> Posts
                            <span class="badge">{{ $nav_posts_count }}</span> &nbsp;</a></li>
                @endif
                @if (Request::user()->hasPermissions([
                    'messages' => ['view']
                ]))
                    <li {{ (Request::is('admin/messages') || Request::is('admin/messages/*') ? 'class=active' : '') }} >
                        <a href="/admin/messages"><i class="glyphicon glyphicon-envelope"></i> Messages
                            <span class="badge">{{ $nav_email_count }}</span> &nbsp;</a></li>
                @endif
                @if (Request::user()->hasPermissions([
                    'users' => ['view']
                ]))
                    <li {{ (Request::is('admin/users') ? 'class=active' : '') }} >
                        <a href="/admin/users"><i class="fa fa-users"></i> Users
                            <span class="badge">{{ $_user::count() }}</span></a></li>
                @endif

                @if (Request::user()->hasPermissions([
                    'config' => ['view']
                ]))
                    <li {{ (Request::is('admin/config') ? 'class=active' : '') }} ><a href="#"><i class="fa fa-cog"></i>
                            Config</a></li>
                @endif
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> {{ Auth::user()->name }}
                        <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/"><i class="glyphicon glyphicon-home"></i> Main Site</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/logout"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
            <!--<ul class="nav navbar-nav navbar-right">
                <li class="dropdown"> <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-collapse-down"></i> Projects <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a target="_blank" href="http://jwelford.co.uk/todo/"><i class="glyphicon glyphicon-log-out"></i> ToDo App</a> </li>
                    </ul>
                </li>
            </ul>-->
        </div>
    </div>
</nav>

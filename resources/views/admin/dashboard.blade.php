@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="well well-custom">
                <h1 style="text-align: center;">There's nothing here yet...</h1>
            </div>
        </div>
    </div>

@stop

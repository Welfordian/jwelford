@extends('layouts.admin')

@section('css')
    <link rel="stylesheet" href="/css/checkbox.css" />
@stop

@section('content')

    @if ( session()->has('status') )
        <div class="alert alert-{{ session()->get('status')['type'] }} alert-dismissable">
            <i class="fa fa-{{ session()->get('status')['icon'] }}"></i>
            {{ session()->get('status')['message'] }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
        </div>
    @endif

    <h1><i class="glyphicon glyphicon-pencil"></i>&nbsp; &nbsp;Editing {{ $user->name }}</h1>

    <h3 style="margin: 2em 0 1em 0;"><i class="fa fa-database"></i>&nbsp; &nbsp;User Details</h3>
    <form method="POST" autocomplete="off">
        <div class="input-group" style="margin-bottom: 16px;">
            <div class="input-group-addon">Name</div>
            <input type="text" name="name" class="form-control" value="{{ $user->name }}" />
        </div>

        <div class="input-group" style="margin-bottom: 16px;">
            <div class="input-group-addon">Email</div>
            <textarea style="height: 48px; resize: none;" type="email" name="email" class="form-control">{{ $user->email }}</textarea>
        </div>

        <div class="input-group" style="margin-bottom: 16px;">
            <div class="input-group-addon">Password</div>
            <input type="password" name="password" autocomplete="new-password" style="position: fixed; left: -2000px;">
            <input type="password" name="password" class="form-control" autocomplete="new-password" />
        </div>

        <div class="input-group">
            <div class="input-group-addon">Repeat Password</div>
            <input type="password" name="password_confirmation" class="form-control" />
        </div>
        @if ( $_user->hasPermission('users', 'change permissions') )
            <h3 style="margin-top: 1em;"><i class="fa fa-bolt"></i>&nbsp; &nbsp;Permissions</h3>

            @foreach ($default_permissions as $master => $children)
                <h4 style="margin: 1em 0 1em 0;">{{ ucfirst($master) }}</h4>
                <div style="margin-left: 2em;">
                    @foreach ($children as $action)
                        @if( $user->hasPermission($master, $action))

                            <div class="checkbox checkbox-success checkbox-inline">
                                <input id="{{$master}}_{{$action}}" class="styled" type="checkbox" name="permissions['{{$master}}']['{{$action}}']" checked>
                                <label for="{{$master}}_{{$action}}">{{ ucfirst($action) }}</label>
                            </div>
                        @else
                            <div class="checkbox checkbox-success checkbox-inline">
                                <input id="{{$master}}_{{$action}}" class="styled" type="checkbox" name="permissions['{{$master}}']['{{$action}}']" />
                                <label for="{{$master}}_{{$action}}">{{ ucfirst($action) }}</label>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endforeach
        @endif
        {{ csrf_field() }}
        <button type="submit" class="btn btn-success pull-right">Save</button>
    </form>
    <style>
        .input-group-addon {
            min-width: 145px;
            text-align: right;
        }

        .checkbox-inline {
            min-width: 85px;
        }
    </style>

@stop
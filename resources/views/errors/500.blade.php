@extends('layouts.app')

@section('content')

    <div class="row-fluid">
        <div class="col-lg-12">
            <div class="centering text-center">
                <div class="text-center">
                    <h2 class="without-margin">Whoops! I can't seem to load that.</h2>
                    <h4 class="text-success">Looks like I messed up-please report this to me.</h4>
                </div>
                <div class="text-center">
                    <h3>
                        <small>Sorry about that, Jim.</small>
                    </h3>
                </div>
                <hr />
                <ul class="pager">
                    <li><a href="/about">&larr; About</a></li>
                    <li><a href="/blog">Blog</a></li>
                    <li><a href="/videos">Videos</a></li>
                    <li><a href="/music">Music</a></li>
                    <li><a href="/contact">Contact &rarr;</a></li>
                </ul>
            </div>
        </div>
    </div>

@stop

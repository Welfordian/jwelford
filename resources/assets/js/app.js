const $ = require('./jquery.js');
const Bootstrap = require('../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.js');
const Vue = require('../../../node_modules/vue/dist/vue.js');
const VueResource = require('../../../node_modules/vue-resource/dist/vue-resource.js');

require('./shims.js');

Vue.use(VueResource);

new Vue({
    el: 'body',
    data: {
      tracks: false,
      videos: false,
      path: document.location.pathname.replace('/', '')
    },
    ready(){
      this.lastFM = this.$resource('https://ws.audioscrobbler.com/2.0/');
      this.unCloak();
      this.getTracks();
      this.getVideos();
      this.contactErrors();
    },
    methods: {
      unCloak(){
        var elems = [].slice.call(document.querySelectorAll('.v-cloak'));
        elems.forEach(function(elem){
          elem.classList.remove('v-cloak')
        })
      },
      bestAlbumImage(images){
        var sizes = [];
    		images.forEach(function (image) {
    			sizes[image['size']] = image[
    				'#text'];
    		});
    		if (sizes['extralarge'].length !== 0) {
    			return sizes['extralarge'];
    		} else if (sizes['large'].length !== 0) {
    			return sizes['large'];
    		} else if (sizes['medium'].length !== 0) {
    			return sizes['medium'];
    		} else if (sizes['small'].length !== 0) {
    			return sizes['small'];
    		} else {
    			return "";
    		}
      },
      getTracks(){
        if(this.path !== "tracks")
          return false;

        this.tracks = false;
        this.lastFM.get({
          method: 'user.getrecenttracks',
          user: 'welfordian',
          api_key: '6136000ba0899c52db5ebcee77d4be15',
          format: 'json'
        }).then((response) => {
          var tracks = JSON.parse(response.body).recenttracks.track;
          for(var i = 0; i < tracks.length; i++){
            tracks[i]['image'] = this.bestAlbumImage(tracks[i]['image']);
          }
          this.tracks = tracks;
        });
      },
      getVideos(){
        if(this.path !== "videos")
              return false;

        var wait_for_firebase = setInterval(function(){
          if('Firebase' in window){
            var videoRef = new Firebase("https://welfordianchat.firebaseio.com/watched_videos");

            videoRef.on('value', function(v){
              var vs = v.val(), videos = [];
              jQuery.each(vs, function(i, v){
                videos.push(v);
              });
              this.videos = videos.slice().reverse();
            }.bind(this));

            clearTimeout(wait_for_firebase);
          }
        }.bind(this), 50);
      },
      contactErrors(){
        if('contactErrors' in window){
          setTimeout(function(){
            jQuery('#errorsModal').modal("show");
          }, 500);
        }
      }
    }
});

global.$ = require('./jquery.js');
const Bootstrap = require('../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.js');
const Vue = require('../../../node_modules/vue/dist/vue.js');
const VueResource = require('../../../node_modules/vue-resource/dist/vue-resource.js');
const pnotify = require('./jquery.pnotify.js');

Vue.use(VueResource);
Vue.http.headers.common['X-CSRF-TOKEN'] = document.getElementById("x-csrf-token").getAttribute("value");
new Vue({
  el: 'body',
  data: {
    path: document.location.pathname.replace('/admin/', ''),
    typeahead: {
      keyTimeout: false,
      emailCollection: false
    },
    newEmail: {
      emailAddress: false
    }
  },
  ready(){
    jQuery.pnotify.defaults.styling = "bootstrap3";
    this.loadUserPermissions();
  },
  methods: {
    loadUserPermissions(){
      console.log(this.$route);
    },
    emailAddressTypeahead(e){
      clearTimeout(this.typeahead.keyTimeout);
      if(this.newEmail.emailAddress.length){
        this.typeahead.keyTimeout = setTimeout(function(){
          this.$http.post('/api/email/search', { email: this.newEmail.emailAddress }).then(function(d){
            this.typeahead.emailCollection = JSON.parse(d.body);
          });
        }.bind(this), 250);
      }else{
        this.typeahead.emailCollection = false;
      }
    },
    sendNewEmail(){
      
    }
  }
});
